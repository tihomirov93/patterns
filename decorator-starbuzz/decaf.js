import { Beverage } from './beverage.js';
const COST = 1.05;
const DESCRIPTION = 'Decaf Cofee';
export class Decaf extends Beverage {
    constructor() {
        super(DESCRIPTION);
    }
    cost() {
        return COST;
    }
}
