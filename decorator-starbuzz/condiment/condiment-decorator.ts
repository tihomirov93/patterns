import { Beverage } from '../beverage.js';

export abstract class CondimentDecorator extends Beverage {
    abstract get description(): string;
}