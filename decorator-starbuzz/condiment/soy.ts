import { Beverage } from '../beverage.js';
import { CondimentDecorator } from './condiment-decorator.js';

const COST = 0.15;
const DESCRIPTION = 'Soy';

export class Soy extends CondimentDecorator {

    constructor(private readonly _beverage: Beverage) {
        super();
    }

    get description(): string {
        return `${this._beverage.description}, ${DESCRIPTION}`; 
    }

    cost(): number {
        return COST + this._beverage.cost();
    }
}