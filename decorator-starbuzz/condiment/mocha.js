import { CondimentDecorator } from './condiment-decorator.js';
const COST = 0.2;
const DESCRIPTION = 'Mocha';
export class Mocha extends CondimentDecorator {
    constructor(_beverage) {
        super();
        this._beverage = _beverage;
    }
    get description() {
        return `${this._beverage.description}, ${DESCRIPTION}`;
    }
    cost() {
        return COST + this._beverage.cost();
    }
}
