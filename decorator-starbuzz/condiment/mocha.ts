import { Beverage } from '../beverage.js';
import { CondimentDecorator } from './condiment-decorator.js';

const COST = 0.2;
const DESCRIPTION = 'Mocha';

export class Mocha extends CondimentDecorator {

    constructor(private readonly _beverage: Beverage) {
        super();
    }

    get description(): string {
        return `${this._beverage.description}, ${DESCRIPTION}`; 
    }

    cost(): number {
        return COST + this._beverage.cost();
    }
}