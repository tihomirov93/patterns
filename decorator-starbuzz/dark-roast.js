import { Beverage } from './beverage.js';
const COST = 0.99;
const DESCRIPTION = 'Most Exellent Dark Roast';
export class DarkRoast extends Beverage {
    constructor() {
        super(DESCRIPTION);
    }
    cost() {
        return COST;
    }
}
