import { Beverage } from './beverage.js';

const COST = 1.99;
const DESCRIPTION = 'Espresso';

export class Espresso extends Beverage {

    constructor() {
        super(DESCRIPTION);
    }

    cost(): number {
        return COST;
    }
}