import { Beverage } from './beverage.js';

const COST = 0.89;
const DESCRIPTION = 'House Blend Cofee';

export class Espresso extends Beverage {

    constructor() {
        super(DESCRIPTION);
    }

    cost(): number {
        return COST;
    }
}