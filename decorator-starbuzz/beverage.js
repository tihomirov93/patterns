export class Beverage {
    constructor(_description = 'Unknown Beverage') {
        this._description = _description;
    }
    get description() {
        return this._description;
    }
}
// const MILK_COST = 0.5;
// const SOY_COST = 0.4;
// const MOCHA_COST = 1.5;
// const WHIP_COST = 0.35;
// export abstract class Beverage {
//     private _milk: boolean = false;
//     private _soy: boolean = false;
//     private _mocha: boolean = false;
//     private _whip: boolean = false;
//     constructor(private readonly _description: string) {}
//     protected cost(): number {
//         let initialCost = 0;
//         if (this.hasMilk()) {
//             initialCost += MILK_COST;
//         }
//         if (this.hasSoy()) {
//             initialCost += SOY_COST;
//         }
//         if (this.hasMocha()) {
//             initialCost += MOCHA_COST;
//         }
//         if (this.hasWhip()) {
//             initialCost += WHIP_COST;
//         }
//         return initialCost
//     }
//     private hasMilk(): boolean {
//         return this._milk;
//     }
//     set milk(milk: boolean) {
//         this._milk = milk;
//     }
//     private hasSoy(): boolean {
//         return this._soy;
//     }
//     set soy(soy: boolean) {
//         this._soy = soy;
//     }
//     private hasMocha(): boolean {
//         return this._mocha;
//     }
//     set mocha(mocha: boolean) {
//         this._mocha = mocha;
//     }
//     private hasWhip(): boolean {
//         return this._whip;
//     }
//     set whip(whip: boolean) {
//         this._whip = whip;
//     }
// }
