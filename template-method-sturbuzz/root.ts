import { Tea, Coffee } from './caffeine-beverage.js';

const tea = new Tea();
tea.prepareRecipe();

console.log('-------------');

const coffee = new Coffee();
coffee.prepareRecipe();