abstract class CaffeineBeverage {
    prepareRecipe(): void {
        this.boilWater();
        this.brew();
        this.pourInCup();
        this.addCondiments();
    }

    abstract brew(): void;

    abstract addCondiments(): void;

    private boilWater(): void {
        console.log('Boil Water');
    }

    private pourInCup(): void {
        console.log('Pour in Cup');
    }
}

export class Tea extends CaffeineBeverage {
    brew(): void {
        console.log('Brew Tea');
    }

    addCondiments(): void {
        console.log('Add Lemon');
    }
}

export class Coffee extends CaffeineBeverage {
    brew(): void {
        console.log('Brew Coffee');
    }

    addCondiments(): void {
        console.log('Add Milk');
    }
}