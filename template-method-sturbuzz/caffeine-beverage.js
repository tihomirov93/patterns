class CaffeineBeverage {
    prepareRecipe() {
        this.boilWater();
        this.brew();
        this.pourInCup();
        this.addCondiments();
    }
    boilWater() {
        console.log('Boil Water');
    }
    pourInCup() {
        console.log('Pour in Cup');
    }
}
export class Tea extends CaffeineBeverage {
    brew() {
        console.log('Brew Tea');
    }
    addCondiments() {
        console.log('Add Lemon');
    }
}
export class Coffee extends CaffeineBeverage {
    brew() {
        console.log('Brew Coffee');
    }
    addCondiments() {
        console.log('Add Milk');
    }
}
