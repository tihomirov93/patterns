import { GumballMashine } from './gumball-mashine.js';

export interface State {
    insertQuarter(): void;
    ejectQuarter(): void;
    turnCrank(): void;
    dispence(): void;
}

export class NoQuarterState implements State {

    constructor(private readonly _gumballMashine: GumballMashine) {

    }

    insertQuarter(): void {
        console.log('You insert a quarter');
        this._gumballMashine.state = this._gumballMashine.hasQuarterState;
    }

    ejectQuarter(): void {
        console.log('You have not inserted a quarter');
    }

    turnCrank(): void {
        console.log('You turned, but there is no quarter');
    }

    dispence(): void {
        console.log('You need to pay first');
    }
}

export class SoldOutState implements State {

    constructor(private readonly _gumballMashine: GumballMashine) {

    }

    insertQuarter(): void {
        console.log('You can not insert a quarter, the mashine is Sold Out');
    }

    ejectQuarter(): void {
        console.log('You can not eject a quarter, the mashine is Sold Out');
    }

    turnCrank(): void {
        console.log('You turned, but there is no gumball');
    }

    dispence(): void {
        console.log('No gumball dispensed');
    }
}

export class HasQuarterState implements State {

    constructor(private readonly _gumballMashine: GumballMashine) {

    }

    insertQuarter(): void {
        console.log('You can not insert another quarter');
    }

    ejectQuarter(): void {
        console.log('Quarter returned');
        this._gumballMashine.state = this._gumballMashine.noQuarterState;
    }

    turnCrank(): void {
        console.log('You turned...');

        if (Math.random() < 0.9 && this._gumballMashine.count > 1) {
            this._gumballMashine.state = this._gumballMashine.winnerState;
        } else {
            this._gumballMashine.state = this._gumballMashine.soldState;
        }
    }

    dispence(): void {
        console.log('No gumball dispensed');
    }
}

export class SoldState implements State {

    constructor(private readonly _gumballMashine: GumballMashine) {

    }

    insertQuarter(): void {
        console.log('Please wait, we are already giving you gumball');
    }

    ejectQuarter(): void {
        console.log('Sorry, you already turned the crank');
    }

    turnCrank(): void {
        console.log('Turning twice does not get you another gumball');
    }

    dispence(): void {
        this._gumballMashine.releaseGumball();

        if (this._gumballMashine.count > 0) {
            this._gumballMashine.state = this._gumballMashine.noQuarterState;
        } else {
            console.log('Oops, out of gumball');
            this._gumballMashine.state = this._gumballMashine.soldOutState;
        }
    }
}

export class WinnerState implements State {

    constructor(private readonly _gumballMashine: GumballMashine) {

    }

    insertQuarter(): void {
        console.log('Please wait, we are already giving you gumball');
    }

    ejectQuarter(): void {
        console.log('Sorry, you already turned the crank');
    }

    turnCrank(): void {
        console.log('Turning twice does not get you another gumball');
    }

    dispence(): void {
        console.log('You are Winner! You get two gumballs fro your quarter');

        this._gumballMashine.releaseGumball();

        if (this._gumballMashine.count === 0) {
            this._gumballMashine.state = this._gumballMashine.soldOutState;
        } else {
            this._gumballMashine.releaseGumball();

            if (this._gumballMashine.count > 0) {
                this._gumballMashine.state = this._gumballMashine.noQuarterState;
            } else {
                console.log('Oops, out of gumball');
                this._gumballMashine.state = this._gumballMashine.soldOutState;
            }
        }
    }
}