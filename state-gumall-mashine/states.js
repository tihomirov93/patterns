export class NoQuarterState {
    constructor(_gumballMashine) {
        this._gumballMashine = _gumballMashine;
    }
    insertQuarter() {
        console.log('You insert a quarter');
        this._gumballMashine.state = this._gumballMashine.hasQuarterState;
    }
    ejectQuarter() {
        console.log('You have not inserted a quarter');
    }
    turnCrank() {
        console.log('You turned, but there is no quarter');
    }
    dispence() {
        console.log('You need to pay first');
    }
}
export class SoldOutState {
    constructor(_gumballMashine) {
        this._gumballMashine = _gumballMashine;
    }
    insertQuarter() {
        console.log('You can not insert a quarter, the mashine is Sold Out');
    }
    ejectQuarter() {
        console.log('You can not eject a quarter, the mashine is Sold Out');
    }
    turnCrank() {
        console.log('You turned, but there is no gumball');
    }
    dispence() {
        console.log('No gumball dispensed');
    }
}
export class HasQuarterState {
    constructor(_gumballMashine) {
        this._gumballMashine = _gumballMashine;
    }
    insertQuarter() {
        console.log('You can not insert another quarter');
    }
    ejectQuarter() {
        console.log('Quarter returned');
        this._gumballMashine.state = this._gumballMashine.noQuarterState;
    }
    turnCrank() {
        console.log('You turned...');
        if (Math.random() < 0.9 && this._gumballMashine.count > 1) {
            this._gumballMashine.state = this._gumballMashine.winnerState;
        }
        else {
            this._gumballMashine.state = this._gumballMashine.soldState;
        }
    }
    dispence() {
        console.log('No gumball dispensed');
    }
}
export class SoldState {
    constructor(_gumballMashine) {
        this._gumballMashine = _gumballMashine;
    }
    insertQuarter() {
        console.log('Please wait, we are already giving you gumball');
    }
    ejectQuarter() {
        console.log('Sorry, you already turned the crank');
    }
    turnCrank() {
        console.log('Turning twice does not get you another gumball');
    }
    dispence() {
        this._gumballMashine.releaseGumball();
        if (this._gumballMashine.count > 0) {
            this._gumballMashine.state = this._gumballMashine.noQuarterState;
        }
        else {
            console.log('Oops, out of gumball');
            this._gumballMashine.state = this._gumballMashine.soldOutState;
        }
    }
}
export class WinnerState {
    constructor(_gumballMashine) {
        this._gumballMashine = _gumballMashine;
    }
    insertQuarter() {
        console.log('Please wait, we are already giving you gumball');
    }
    ejectQuarter() {
        console.log('Sorry, you already turned the crank');
    }
    turnCrank() {
        console.log('Turning twice does not get you another gumball');
    }
    dispence() {
        console.log('You are Winner! You get two gumballs fro your quarter');
        this._gumballMashine.releaseGumball();
        if (this._gumballMashine.count === 0) {
            this._gumballMashine.state = this._gumballMashine.soldOutState;
        }
        else {
            this._gumballMashine.releaseGumball();
            if (this._gumballMashine.count > 0) {
                this._gumballMashine.state = this._gumballMashine.noQuarterState;
            }
            else {
                console.log('Oops, out of gumball');
                this._gumballMashine.state = this._gumballMashine.soldOutState;
            }
        }
    }
}
