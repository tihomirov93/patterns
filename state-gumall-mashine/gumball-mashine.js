import { NoQuarterState, SoldOutState, HasQuarterState, SoldState, WinnerState } from './states.js';
export class GumballMashine {
    constructor(_count = 0) {
        this._count = _count;
        this._noQuarterState = new NoQuarterState(this);
        this._hasQuarterState = new HasQuarterState(this);
        this._soldOutState = new SoldOutState(this);
        this._soldState = new SoldState(this);
        this._winnerState = new WinnerState(this);
        this._state = this._soldOutState;
        this._noQuarterState = new NoQuarterState(this);
        if (_count > 0) {
            this._state = this._noQuarterState;
        }
    }
    get count() {
        return this._count;
    }
    set count(c) {
        this._count = c;
    }
    get noQuarterState() {
        return this._noQuarterState;
    }
    get hasQuarterState() {
        return this._hasQuarterState;
    }
    get soldOutState() {
        return this._soldOutState;
    }
    get soldState() {
        return this._soldState;
    }
    get winnerState() {
        return this._winnerState;
    }
    set state(s) {
        this._state = s;
    }
    insertQuarter() {
        this._state.insertQuarter();
    }
    ejectQuarter() {
        this._state.ejectQuarter();
    }
    turnCrank() {
        this._state.turnCrank();
        this._state.dispence();
    }
    releaseGumball() {
        console.log('A gumball comes rolling out the slots');
        if (this.count !== 0) {
            this.count -= 1;
        }
    }
    toString() {
        return `Gumball Mashine Status. State - ${this._state}, Count - ${this._count}`;
    }
}
