import { State, NoQuarterState, SoldOutState, HasQuarterState, SoldState, WinnerState } from './states.js';    

export class GumballMashine {
    private readonly _noQuarterState = new NoQuarterState(this);
    private readonly _hasQuarterState = new HasQuarterState(this);
    private readonly _soldOutState = new SoldOutState(this);
    private readonly _soldState = new SoldState(this);
    private readonly _winnerState = new WinnerState(this);

    private _state: State = this._soldOutState;

    constructor(private _count = 0) {
        this._noQuarterState = new NoQuarterState(this);

        if (_count > 0) {
            this._state = this._noQuarterState;
        }
    }

    get count(): number {
        return this._count;
    }

    set count(c: number) {
        this._count = c;
    }

    get noQuarterState(): State {
        return this._noQuarterState;
    }

    get hasQuarterState(): State {
        return this._hasQuarterState;
    }

    get soldOutState(): State {
        return this._soldOutState;
    }

    get soldState(): State {
        return this._soldState;
    }

    get winnerState(): State {
        return this._winnerState;
    }

    set state(s: State) {
        this._state = s;
    }

    insertQuarter(): void {
        this._state.insertQuarter();
    }

    ejectQuarter(): void {
        this._state.ejectQuarter();
    }

    turnCrank(): void {
        this._state.turnCrank();
        this._state.dispence();
    }

    releaseGumball(): void {
        console.log('A gumball comes rolling out the slots');
        if (this.count !== 0) {
            this.count -= 1;
        }
    }

    toString(): string {
        return `Gumball Mashine Status. State - ${this._state}, Count - ${this._count}`;
    }
}
