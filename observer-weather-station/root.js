import { WeatherData } from './weather-data.js';
import { CurrentCondition } from './current-condition.js';
const weatherData = new WeatherData();
const currentCondition = new CurrentCondition(weatherData);
weatherData.setMeasurements(25, 55, 870);
weatherData.setMeasurements(26, 60, 870);
weatherData.setMeasurements(22, 40, 860);
