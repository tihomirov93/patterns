export interface Observer {
    update(temp: number, humidity: number, pressure: number): void;
}

export interface Subject {
    registerObserver(observer: Observer): void;
    removeObserver(oserver: Observer): void;
    notifyObservers(): void
}

export interface DisplayElement {
    display(): void;
}