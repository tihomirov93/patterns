import { Observer, DisplayElement, Subject } from './types.js';

export class CurrentCondition implements Observer, DisplayElement {
    private _temp: number;
    private _humidity: number;

    constructor(weatherData: Subject) {
        weatherData.registerObserver(this);
    }

    update(temp: number, humidity: number): void {
        this._temp = temp;
        this._humidity = humidity;

        this.display();
    }

    display(): void {
        console.log(`Current Condition: tempreture = ${this._temp}, humidity = ${this._humidity}.`);
    }
}