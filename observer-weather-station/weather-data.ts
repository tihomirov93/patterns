import { Observer, Subject } from './types.js';

export class WeatherData implements Subject {
    private readonly _observers: Array<Observer> = [];
    private _temp: number;
    private _humidity: number;
    private _pressure: number;

    registerObserver(oserver: Observer): void {
        this._observers.push(oserver);
    }

    removeObserver(oserver: Observer): void {
        const observerIndex = this._observers.findIndex(o => o === oserver);

        if (observerIndex !== -1) {
            this._observers.splice(observerIndex, 1)
        } else {
            console.error('This oserver is not registered');
        }
    }

    notifyObservers(): void {
        this._observers.forEach(observer => {
            observer.update(this._temp, this._humidity, this._pressure)
        })
    }

    public setMeasurements(temp: number, humidity: number, pressure: number): void {
        this._temp = temp;
        this._humidity = humidity;
        this._pressure = pressure;

        this.measurementsChanged();
    }

    private measurementsChanged(): void {
        this.notifyObservers();
    }
}