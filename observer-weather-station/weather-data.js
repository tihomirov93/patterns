export class WeatherData {
    constructor() {
        this._observers = [];
    }
    registerObserver(oserver) {
        this._observers.push(oserver);
    }
    removeObserver(oserver) {
        const observerIndex = this._observers.findIndex(o => o === oserver);
        if (observerIndex !== -1) {
            this._observers.splice(observerIndex, 1);
        }
        else {
            console.error('This oserver is not registered');
        }
    }
    notifyObservers() {
        this._observers.forEach(observer => {
            observer.update(this._temp, this._humidity, this._pressure);
        });
    }
    setMeasurements(temp, humidity, pressure) {
        this._temp = temp;
        this._humidity = humidity;
        this._pressure = pressure;
        this.measurementsChanged();
    }
    measurementsChanged() {
        this.notifyObservers();
    }
}
