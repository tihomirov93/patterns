export class CurrentCondition {
    constructor(weatherData) {
        weatherData.registerObserver(this);
    }
    update(temp, humidity) {
        this._temp = temp;
        this._humidity = humidity;
        this.display();
    }
    display() {
        console.log(`Current Condition: tempreture = ${this._temp}, humidity = ${this._humidity}.`);
    }
}
