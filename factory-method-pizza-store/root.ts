import { PizzaType } from './pizza.js';
import { NYStylePizzaStore } from './ny-style-pizza-store.js';
import { ChicagoStylePizzaStore } from './chicago-style-pizza-store.js';

const nyStylePizzaStore = new NYStylePizzaStore();
const chicagoStylePizzaStore = new ChicagoStylePizzaStore();

nyStylePizzaStore.orderPizza(PizzaType.Cheese);
chicagoStylePizzaStore.orderPizza(PizzaType.Paperroni);