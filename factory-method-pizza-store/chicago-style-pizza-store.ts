import { PizzaStore } from './pizza-store.js';
import { PizzaType, Pizza, CheesePizza, PapperoniPizza, ClamPizza, VeggiePizza } from './pizza.js';
import { ChicagoPizzaIngredientFactory } from './pizza-ingredient-factory.js';

export class ChicagoStylePizzaStore extends PizzaStore {

    createPizza(pizzaType: PizzaType): Pizza {
        const pizzaIngredientFactory = new ChicagoPizzaIngredientFactory();
        switch(pizzaType) {
            case PizzaType.Cheese:
                return new CheesePizza(pizzaIngredientFactory);
            case PizzaType.Paperroni:
                return new PapperoniPizza(pizzaIngredientFactory);
            case PizzaType.Clam:
                return new ClamPizza(pizzaIngredientFactory);
            case PizzaType.Veggie:
                return new VeggiePizza(pizzaIngredientFactory);
        }
    };
}
