import { PizzaType, Pizza } from './pizza.js';

export abstract class PizzaStore {

    orderPizza(pizzaType: PizzaType): Pizza {
        const pizza = this.createPizza(pizzaType);

        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }

    abstract createPizza(pizzaType: PizzaType): Pizza;
}