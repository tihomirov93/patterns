import { Dough, Sauce, Cheese, Papperoni, Clam, Veggies, PizzaIngredientFactory } from './pizza-ingredient-factory.js'

export enum PizzaType {
    Cheese,
    Paperroni,
    Clam,
    Veggie,
}

export abstract class Pizza {
    protected dough: Dough;
    protected sauce: Sauce;
    protected cheese: Cheese;
    protected papperoni: Papperoni;
    protected clam: Clam;
    protected veggies: ReadonlyArray<Veggies>;

    abstract prepare(): void;

    constructor(private readonly _type: PizzaType) {
    }

    bake(): void {
        console.log('Bake for 25 minutes');
    }
    cut(): void {
        console.log('Cutting the pizza into diagonal slices');
    }
    box(): void {
        console.log('Pizza placed in official PizzaStore box');
    }

    get name(): string {
        return PizzaType[this._type];
    }
};

export class ClamPizza extends Pizza {

    constructor(private readonly _pizzaIngredientFactory: PizzaIngredientFactory) {
        super(PizzaType.Clam);
    }

    prepare(): void {
        console.log(`Preparing ${this.name}`);
        this.dough = this._pizzaIngredientFactory.createDough();
        this.sauce = this._pizzaIngredientFactory.createSauce();
        this.cheese = this._pizzaIngredientFactory.createChesse();
        this.clam = this._pizzaIngredientFactory.createClam();
    }
}

export class VeggiePizza extends Pizza {

    constructor(private readonly _pizzaIngredientFactory: PizzaIngredientFactory) {
        super(PizzaType.Veggie);
    }

    prepare(): void {
        console.log(`Preparing ${this.name}`);
        this.dough = this._pizzaIngredientFactory.createDough();
        this.sauce = this._pizzaIngredientFactory.createSauce();
        this.cheese = this._pizzaIngredientFactory.createChesse();
        this.veggies = this._pizzaIngredientFactory.createVeggies();
    }
}

export class CheesePizza extends Pizza {

    constructor(private readonly _pizzaIngredientFactory: PizzaIngredientFactory) {
        super(PizzaType.Cheese);
    }

    prepare(): void {
        console.log(`Preparing ${this.name}`);
        this.dough = this._pizzaIngredientFactory.createDough();
        this.sauce = this._pizzaIngredientFactory.createSauce();
        this.cheese = this._pizzaIngredientFactory.createChesse();
    }
}

export class PapperoniPizza extends Pizza {

    constructor(private readonly _pizzaIngredientFactory: PizzaIngredientFactory) {
        super(PizzaType.Paperroni);
    }

    prepare(): void {
        console.log(`Preparing ${this.name}`);
        this.dough = this._pizzaIngredientFactory.createDough();
        this.sauce = this._pizzaIngredientFactory.createSauce();
        this.papperoni = this._pizzaIngredientFactory.createPapperoni();
        this.cheese = this._pizzaIngredientFactory.createChesse();
    }
}
