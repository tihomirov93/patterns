export class NYPizzaIngredientFactory {
    createDough() {
        return 0 /* ThinCrust */;
    }
    createSauce() {
        return 0 /* Marinara */;
    }
    createChesse() {
        return 0 /* Reggiano */;
    }
    createPapperoni() {
        return 0 /* Sliced */;
    }
    createClam() {
        return 0 /* Fresh */;
    }
    createVeggies() {
        return [0 /* Garlic */, 1 /* Onion */, 2 /* Mushroom */, 3 /* RedPepper */];
    }
}
export class ChicagoPizzaIngredientFactory {
    createDough() {
        return 1 /* ThickCrust */;
    }
    createSauce() {
        return 1 /* PlumTomato */;
    }
    createChesse() {
        return 1 /* Mozzarella */;
    }
    createPapperoni() {
        return 0 /* Sliced */;
    }
    createClam() {
        return 1 /* Frozen */;
    }
    createVeggies() {
        return [4 /* BlackOlives */, 5 /* Spinach */, 6 /* EggPlant */];
    }
}
