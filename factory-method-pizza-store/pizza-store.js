export class PizzaStore {
    orderPizza(pizzaType) {
        const pizza = this.createPizza(pizzaType);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
