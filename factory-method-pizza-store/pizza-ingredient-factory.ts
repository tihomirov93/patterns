export const enum Dough {
    ThinCrust,
    ThickCrust,
}

export const enum Sauce {
    Marinara,
    PlumTomato,
}

export const enum Cheese {
    Reggiano,
    Mozzarella,
}

export const enum Papperoni {
    Sliced,
}

export const enum Clam {
    Fresh,
    Frozen,
}

export const enum Veggies {
    Garlic,
    Onion,
    Mushroom,
    RedPepper,
    BlackOlives,
    Spinach,
    EggPlant,
}

export interface PizzaIngredientFactory {
    createDough(): Dough;
    createSauce(): Sauce;
    createChesse(): Cheese;
    createPapperoni(): Papperoni;
    createClam(): Clam;
    createVeggies(): ReadonlyArray<Veggies>;
}

export class NYPizzaIngredientFactory implements PizzaIngredientFactory {
    createDough(): Dough {
        return Dough.ThinCrust;
    }
    createSauce(): Sauce {
        return Sauce.Marinara;
    }
    createChesse(): Cheese  {
        return Cheese.Reggiano;
    }
    createPapperoni(): Papperoni {
        return Papperoni.Sliced;
    }
    createClam(): Clam {
        return Clam.Fresh;
    }
    createVeggies(): ReadonlyArray<Veggies> {
        return [Veggies.Garlic, Veggies.Onion, Veggies.Mushroom, Veggies.RedPepper];
    }
}

export class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {
    createDough(): Dough {
        return Dough.ThickCrust;
    }
    createSauce(): Sauce {
        return Sauce.PlumTomato;
    }
    createChesse(): Cheese  {
        return Cheese.Mozzarella;
    }
    createPapperoni(): Papperoni {
        return Papperoni.Sliced;
    }
    createClam(): Clam {
        return Clam.Frozen;
    }
    createVeggies(): ReadonlyArray<Veggies> {
        return [Veggies.BlackOlives, Veggies.Spinach, Veggies.EggPlant];
    }
}