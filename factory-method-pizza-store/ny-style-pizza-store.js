import { PizzaStore } from './pizza-store.js';
import { PizzaType, CheesePizza, PapperoniPizza, ClamPizza, VeggiePizza } from './pizza.js';
import { NYPizzaIngredientFactory } from './pizza-ingredient-factory.js';
export class NYStylePizzaStore extends PizzaStore {
    createPizza(pizzaType) {
        const pizzaIngredientFactory = new NYPizzaIngredientFactory();
        switch (pizzaType) {
            case PizzaType.Cheese:
                return new CheesePizza(pizzaIngredientFactory);
            case PizzaType.Paperroni:
                return new PapperoniPizza(pizzaIngredientFactory);
            case PizzaType.Clam:
                return new ClamPizza(pizzaIngredientFactory);
            case PizzaType.Veggie:
                return new VeggiePizza(pizzaIngredientFactory);
        }
    }
    ;
}
