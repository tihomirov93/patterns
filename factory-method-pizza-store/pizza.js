export var PizzaType;
(function (PizzaType) {
    PizzaType[PizzaType["Cheese"] = 0] = "Cheese";
    PizzaType[PizzaType["Paperroni"] = 1] = "Paperroni";
    PizzaType[PizzaType["Clam"] = 2] = "Clam";
    PizzaType[PizzaType["Veggie"] = 3] = "Veggie";
})(PizzaType || (PizzaType = {}));
export class Pizza {
    constructor(_type) {
        this._type = _type;
    }
    bake() {
        console.log('Bake for 25 minutes');
    }
    cut() {
        console.log('Cutting the pizza into diagonal slices');
    }
    box() {
        console.log('Pizza placed in official PizzaStore box');
    }
    get name() {
        return PizzaType[this._type];
    }
}
;
export class ClamPizza extends Pizza {
    constructor(_pizzaIngredientFactory) {
        super(PizzaType.Clam);
        this._pizzaIngredientFactory = _pizzaIngredientFactory;
    }
    prepare() {
        console.log(`Preparing ${this.name}`);
        this.dough = this._pizzaIngredientFactory.createDough();
        this.sauce = this._pizzaIngredientFactory.createSauce();
        this.cheese = this._pizzaIngredientFactory.createChesse();
        this.clam = this._pizzaIngredientFactory.createClam();
    }
}
export class VeggiePizza extends Pizza {
    constructor(_pizzaIngredientFactory) {
        super(PizzaType.Veggie);
        this._pizzaIngredientFactory = _pizzaIngredientFactory;
    }
    prepare() {
        console.log(`Preparing ${this.name}`);
        this.dough = this._pizzaIngredientFactory.createDough();
        this.sauce = this._pizzaIngredientFactory.createSauce();
        this.cheese = this._pizzaIngredientFactory.createChesse();
        this.veggies = this._pizzaIngredientFactory.createVeggies();
    }
}
export class CheesePizza extends Pizza {
    constructor(_pizzaIngredientFactory) {
        super(PizzaType.Cheese);
        this._pizzaIngredientFactory = _pizzaIngredientFactory;
    }
    prepare() {
        console.log(`Preparing ${this.name}`);
        this.dough = this._pizzaIngredientFactory.createDough();
        this.sauce = this._pizzaIngredientFactory.createSauce();
        this.cheese = this._pizzaIngredientFactory.createChesse();
    }
}
export class PapperoniPizza extends Pizza {
    constructor(_pizzaIngredientFactory) {
        super(PizzaType.Paperroni);
        this._pizzaIngredientFactory = _pizzaIngredientFactory;
    }
    prepare() {
        console.log(`Preparing ${this.name}`);
        this.dough = this._pizzaIngredientFactory.createDough();
        this.sauce = this._pizzaIngredientFactory.createSauce();
        this.papperoni = this._pizzaIngredientFactory.createPapperoni();
        this.cheese = this._pizzaIngredientFactory.createChesse();
    }
}
