export class Singleton {
    static getInstance(): Singleton {
        if (Singleton._instance === undefined) {
            Singleton._instance = new Singleton();
        }

        return Singleton._instance;
    }

    private static _instance: Singleton | undefined = undefined;

    private constructor (){
    }
}
