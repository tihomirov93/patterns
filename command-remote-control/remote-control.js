class NoCommand {
    execute() {
        console.log('Command is not set');
    }
    undo() {
        console.log('Nothing to undo');
    }
}
export class RemoteControl {
    constructor() {
        this._onCommands = [];
        this._offCommands = [];
        const noCommand = new NoCommand();
        for (let i = 0; i < 7; i++) {
            this._onCommands[i] = noCommand;
            this._offCommands[i] = noCommand;
        }
        this._undoCommand = noCommand;
    }
    setCommand(slot, onCommand, offComand) {
        this._onCommands[slot] = onCommand;
        this._offCommands[slot] = offComand;
    }
    onButtonWasPushed(slot) {
        const command = this._onCommands[slot];
        command.execute();
        this._undoCommand = command;
    }
    offButtonWasPushed(slot) {
        const command = this._offCommands[slot];
        command.execute();
        this._undoCommand = command;
    }
    undoButtonWasPushed() {
        this._undoCommand.undo();
    }
}
