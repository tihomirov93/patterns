import { Command } from './command.js';

export class GarageDoor {
    up(): void {
        console.log('Garage Door Up');
    }
    down(): void {
        console.log('Garage Door Down')
    }
    stop(): void {
        console.log('Garage Door Stop')
    }
    lightOn(): void {
        console.log('Garage Door Light On')
    }
    lightOff(): void {
        console.log('Garage Door Light Off')
    }
}

export class GarageDoorOpenCommand implements Command {
    constructor(private readonly _garageDoor: GarageDoor){

    }
    execute(): void {
        this._garageDoor.lightOn();
        this._garageDoor.up();
    }
    undo(): void {
        this._garageDoor.lightOff();
        this._garageDoor.down();
    }
}

export class GarageDoorCloseCommand implements Command {
    constructor(private readonly _garageDoor: GarageDoor){

    }
    execute(): void {
        this._garageDoor.lightOff();
        this._garageDoor.down();
    }
    undo(): void {
        this._garageDoor.lightOn();
        this._garageDoor.up();
    }
}