export class Stereo {
    on() {
        console.log('Stereo On');
    }
    off() {
        console.log('Stereo Off');
    }
    setCD() {
        console.log('Stereo Set CD');
    }
    setDVD() {
        console.log('Stereo Set DVD');
    }
    setRadio() {
        console.log('Stereo Set Radio');
    }
    setVolue(volume) {
        console.log('Stereo Set Volume');
    }
}
export class StereoOnWithCDCommand {
    constructor(_stereo) {
        this._stereo = _stereo;
    }
    execute() {
        this._stereo.on();
        this._stereo.setCD();
        this._stereo.setVolue(11);
    }
    undo() {
        this._stereo.off();
    }
}
export class StereoOffCommand {
    constructor(_stereo) {
        this._stereo = _stereo;
    }
    execute() {
        this._stereo.off();
    }
    undo() {
        this._stereo.on();
        this._stereo.setCD();
        this._stereo.setVolue(11);
    }
}
