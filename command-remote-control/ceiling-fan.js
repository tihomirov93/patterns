export class CeilingFan {
    constructor(_roomName) {
        this._roomName = _roomName;
    }
    on() {
        console.log(`Ceiling Fan On, ${this._roomName}`);
    }
    off() {
        console.log(`Ceiling Fan Off, ${this._roomName}`);
    }
    dimm() {
        console.log(`Ceiling Fan Off, ${this._roomName}`);
    }
}
export class CeilingFanOnCommand {
    constructor(_ceilingFan) {
        this._ceilingFan = _ceilingFan;
    }
    execute() {
        this._ceilingFan.on();
    }
    undo() {
        this._ceilingFan.off();
    }
}
export class CeilingFanOffCommand {
    constructor(_ceilingFan) {
        this._ceilingFan = _ceilingFan;
    }
    execute() {
        this._ceilingFan.on();
    }
    undo() {
        this._ceilingFan.off();
    }
}
