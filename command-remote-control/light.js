export class Light {
    constructor(_roomName) {
        this._roomName = _roomName;
    }
    on() {
        console.log(`Light On, ${this._roomName}`);
    }
    off() {
        console.log(`Light Off, ${this._roomName}`);
    }
}
export class LightOnCommand {
    constructor(_light) {
        this._light = _light;
    }
    execute() {
        this._light.on();
    }
    undo() {
        this._light.off();
    }
}
export class LightOffCommand {
    constructor(_light) {
        this._light = _light;
    }
    execute() {
        this._light.off();
    }
    undo() {
        this._light.on();
    }
}
