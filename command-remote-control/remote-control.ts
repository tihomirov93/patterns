import { Command } from './command.js';

class NoCommand implements Command {
    execute(): void {
        console.log('Command is not set');
    }
    undo(): void {
        console.log('Nothing to undo')
    }
}

export class RemoteControl {
    private readonly _onCommands: Command[] = [];
    private readonly _offCommands: Command[] = [];
    private _undoCommand: Command;

    constructor() {
        const noCommand = new NoCommand();

        for (let i = 0; i < 7; i++) {
            this._onCommands[i] = noCommand;
            this._offCommands[i] = noCommand;
        }

        this._undoCommand = noCommand;
    }

    setCommand(slot: number, onCommand: Command, offComand: Command): void {
        this._onCommands[slot] = onCommand;
        this._offCommands[slot] = offComand;
    }

    onButtonWasPushed(slot: number): void {
        const command = this._onCommands[slot];
        command.execute();
        this._undoCommand = command;
    }

    offButtonWasPushed(slot: number): void {
        const command = this._offCommands[slot];
        command.execute();
        this._undoCommand = command;
    }

    undoButtonWasPushed(): void {
        this._undoCommand.undo();
    }
}