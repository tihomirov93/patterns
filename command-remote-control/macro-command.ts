import { Command } from './command.js';

export class MacroCommand implements Command {
    constructor(private readonly _commands: Command[]){

    }
    execute(): void {
        this._commands.forEach(command => command.execute());
    }
    undo(): void {
        this._commands.forEach(command => command.undo());
    }
}
