import { Command } from './command.js';

export class CeilingFan {
    constructor(private readonly _roomName: string){

    }
    on(): void {
        console.log(`Ceiling Fan On, ${this._roomName}`);
    }
    off(): void {
        console.log(`Ceiling Fan Off, ${this._roomName}`);
    }
    dimm(): void {
        console.log(`Ceiling Fan Off, ${this._roomName}`);
    }
}

export class CeilingFanOnCommand implements Command {
    constructor(private readonly _ceilingFan: CeilingFan){

    }
    execute(): void {
        this._ceilingFan.on();
    }
    undo(): void {
        this._ceilingFan.off();
    }
}

export class CeilingFanOffCommand implements Command {
    constructor(private readonly _ceilingFan: CeilingFan){

    }
    execute(): void {
        this._ceilingFan.on();
    }
    undo(): void {
        this._ceilingFan.off();
    }
}