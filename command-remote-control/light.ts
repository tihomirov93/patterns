import { Command } from './command.js';

export class Light {
    constructor(private readonly _roomName: string){

    }
    on(): void {
        console.log(`Light On, ${this._roomName}`);
    }
    off(): void {
        console.log(`Light Off, ${this._roomName}`);
    }
}

export class LightOnCommand implements Command {
    constructor(private readonly _light: Light){

    }
    execute(): void {
        this._light.on();
    }
    undo(): void {
        this._light.off();
    }
}

export class LightOffCommand implements Command {
    constructor(private readonly _light: Light){

    }
    execute(): void {
        this._light.off();
    }
    undo(): void {
        this._light.on();
    }
}