export class GarageDoor {
    up() {
        console.log('Garage Door Up');
    }
    down() {
        console.log('Garage Door Down');
    }
    stop() {
        console.log('Garage Door Stop');
    }
    lightOn() {
        console.log('Garage Door Light On');
    }
    lightOff() {
        console.log('Garage Door Light Off');
    }
}
export class GarageDoorOpenCommand {
    constructor(_garageDoor) {
        this._garageDoor = _garageDoor;
    }
    execute() {
        this._garageDoor.lightOn();
        this._garageDoor.up();
    }
    undo() {
        this._garageDoor.lightOff();
        this._garageDoor.down();
    }
}
export class GarageDoorCloseCommand {
    constructor(_garageDoor) {
        this._garageDoor = _garageDoor;
    }
    execute() {
        this._garageDoor.lightOff();
        this._garageDoor.down();
    }
    undo() {
        this._garageDoor.lightOn();
        this._garageDoor.up();
    }
}
