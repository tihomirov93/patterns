export class MacroCommand {
    constructor(_commands) {
        this._commands = _commands;
    }
    execute() {
        this._commands.forEach(command => command.execute());
    }
    undo() {
        this._commands.forEach(command => command.undo());
    }
}
