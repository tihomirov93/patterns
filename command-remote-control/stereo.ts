import { Command } from './command.js';

export class Stereo {
    on(): void {
        console.log('Stereo On');
    }
    off(): void {
        console.log('Stereo Off')
    }
    setCD(): void {
        console.log('Stereo Set CD')
    }
    setDVD(): void {
        console.log('Stereo Set DVD')
    }
    setRadio(): void {
        console.log('Stereo Set Radio')
    }
    setVolue(volume: number): void {
        console.log('Stereo Set Volume')
    }
}

export class StereoOnWithCDCommand implements Command {
    constructor(private readonly _stereo: Stereo){

    }
    execute(): void {
        this._stereo.on();
        this._stereo.setCD();
        this._stereo.setVolue(11);
    }
    undo(): void {
        this._stereo.off();
    }
}

export class StereoOffCommand implements Command {
    constructor(private readonly _stereo: Stereo){

    }
    execute(): void {
        this._stereo.off();
    }
    undo(): void {
        this._stereo.on();
        this._stereo.setCD();
        this._stereo.setVolue(11);
    }
}