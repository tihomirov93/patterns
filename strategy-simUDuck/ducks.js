import { Duck } from './duck.js';
import { FlyWithWings, FlyNoWay } from './fly.js';
import { Quack } from './quack.js';
export class MallardDuck extends Duck {
    constructor() {
        super(new FlyWithWings(), new Quack());
    }
    display() {
        console.log('Display Mallard Duck');
    }
}
export class ModelDuck extends Duck {
    constructor() {
        super(new FlyNoWay(), new Quack());
    }
    display() {
        console.log('Display Model Duck');
    }
}
