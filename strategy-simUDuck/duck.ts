import { FlyBehaviour } from './fly.js';
import { QuackBehaviour } from './quack.js';

export abstract class Duck {
    abstract display(): void;

    constructor(
        private _flyBehaviour: FlyBehaviour,
        private _quackBehaviour: QuackBehaviour,
    ) {}

    swim(): void {
        console.log('I can swim');
    }

    performFly(): void {
        this._flyBehaviour.fly();
    }

    performQuack(): void {
        this._quackBehaviour.quack();
    }

    setFlyBehavoiur(flyBehaviour: FlyBehaviour): void {
        this._flyBehaviour = flyBehaviour;
    }

    setQuackBehavoiur(quackBehaviour: QuackBehaviour): void {
        this._quackBehaviour = quackBehaviour;
    }
}
