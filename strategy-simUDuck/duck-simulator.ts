import { MallardDuck, ModelDuck } from './ducks.js';
import { FlyRockedPowered } from './fly.js';

const mallardDuck = new MallardDuck();

mallardDuck.performFly();
mallardDuck.performQuack();

const modelDuck = new ModelDuck();

modelDuck.performFly();
modelDuck.setFlyBehavoiur(new FlyRockedPowered());
modelDuck.performFly();