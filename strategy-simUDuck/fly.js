export class FlyWithWings {
    fly() {
        console.log('Fly with wings');
    }
}
export class FlyNoWay {
    fly() {
        console.log('I can not fly');
    }
}
export class FlyRockedPowered {
    fly() {
        console.log('Fly with a Rocket');
    }
}
