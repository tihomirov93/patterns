export class Duck {
    constructor(_flyBehaviour, _quackBehaviour) {
        this._flyBehaviour = _flyBehaviour;
        this._quackBehaviour = _quackBehaviour;
    }
    swim() {
        console.log('I can swim');
    }
    performFly() {
        this._flyBehaviour.fly();
    }
    performQuack() {
        this._quackBehaviour.quack();
    }
    setFlyBehavoiur(flyBehaviour) {
        this._flyBehaviour = flyBehaviour;
    }
    setQuackBehavoiur(quackBehaviour) {
        this._quackBehaviour = quackBehaviour;
    }
}
