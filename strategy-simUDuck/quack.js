export class Quack {
    quack() {
        console.log('Quack Quack Quack');
    }
}
export class Squeak {
    quack() {
        console.log('Squeak');
    }
}
export class MuteQuack {
    quack() {
        console.log('I can not Quack');
    }
}
