export interface FlyBehaviour {
    fly(): void;
}

export class FlyWithWings implements FlyBehaviour {
    fly(): void {
        console.log('Fly with wings');
    }
}

export class FlyNoWay implements FlyBehaviour {
    fly(): void {
        console.log('I can not fly');
    }
}

export class FlyRockedPowered implements FlyBehaviour {
    fly(): void {
        console.log('Fly with a Rocket');
    }
}